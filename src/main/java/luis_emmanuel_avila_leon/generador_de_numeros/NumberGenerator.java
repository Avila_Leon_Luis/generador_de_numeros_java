package luis_emmanuel_avila_leon.generador_de_numeros;

    import java.lang.Math;  
/**
 *
 * @author Luis Emmanuel
 */
public class NumberGenerator {
    
    public static int generateNumber(int maxValue, int minValue){
       return (int) (Math.random() * (maxValue + 1 - minValue)) + minValue;

    }
    
}
  